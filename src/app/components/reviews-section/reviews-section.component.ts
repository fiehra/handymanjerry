import { Component } from '@angular/core';

@Component({
  selector: 'reviews-section',
  templateUrl: './reviews-section.component.html',
  styleUrls: ['./reviews-section.component.scss'],
})
export class ReviewsSectionComponent {}
