import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'contact-section',
  templateUrl: './contact-section.component.html',
  styleUrls: ['./contact-section.component.scss'],
})
export class ContactSectionComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  copyEmail() {
    const invisCopyLink = document.createElement('textarea');
    invisCopyLink.style.position = 'fixed';
    invisCopyLink.style.left = '0';
    invisCopyLink.style.top = '0';
    invisCopyLink.style.opacity = '0';
    invisCopyLink.value = 'handymanjerryberlin@gmail.com';
    document.body.appendChild(invisCopyLink);
    invisCopyLink.focus();
    invisCopyLink.select();
    document.execCommand('copy');
    document.body.removeChild(invisCopyLink);
    this.showSnackbar();
  }

  showSnackbar() {
    // Get the snackbar DIV
    var x = document.getElementById('snackbar');

    // Add the "show" class to DIV
    x!.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(function () {
      x!.className = x!.className.replace('show', '');
    }, 3000);
  }
}
