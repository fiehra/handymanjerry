import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HeaderComponent } from './header/header.component';
import { ReviewsSectionComponent } from './reviews-section/reviews-section.component';
import { FooterComponent } from './footer/footer.component';
import { InfoSectionComponent } from './info-section/info-section.component';
import { PricesSectionComponent } from './prices-section/prices-section.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { AboutSectionComponent } from './about-section/about-section.component';
import { ContactSectionComponent } from './contact-section/contact-section.component';

@NgModule({
  declarations: [
    AboutSectionComponent,
    ToolbarComponent,
    HeaderComponent,
    ReviewsSectionComponent,
    FooterComponent,
    InfoSectionComponent,
    PricesSectionComponent,
    ScrollTopComponent,
    ContactSectionComponent,
  ],
  imports: [FormsModule, ReactiveFormsModule, CommonModule, RouterModule],
  exports: [
    AboutSectionComponent,
    ToolbarComponent,
    ContactSectionComponent,
    HeaderComponent,
    ReviewsSectionComponent,
    FooterComponent,
    InfoSectionComponent,
    PricesSectionComponent,
    ScrollTopComponent,
  ],
})
export class ComponentsModule {}
