import { Component } from '@angular/core';

@Component({
  selector: 'prices-section',
  templateUrl: './prices-section.component.html',
  styleUrls: ['./prices-section.component.scss'],
})
export class PricesSectionComponent {}
