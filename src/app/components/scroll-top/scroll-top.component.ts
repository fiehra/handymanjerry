import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'scroll-top',
  templateUrl: './scroll-top.component.html',
  styleUrls: ['./scroll-top.component.scss'],
})
export class ScrollTopComponent implements OnInit {
  showScrollButton: boolean = false;

  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {}

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (
      (window.pageYOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop) > 764
    ) {
      this.showScrollButton = true;
    } else if (
      this.showScrollButton &&
      (window.pageYOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop) < 764
    ) {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    (function smoothscroll() {
      let currentScroll =
        document.body.scrollTop || document.documentElement.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothscroll);
        window.scrollTo(0, currentScroll - currentScroll / 8);
      }
    })();
  }
}
