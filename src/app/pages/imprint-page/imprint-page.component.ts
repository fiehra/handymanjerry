import { Component } from '@angular/core';

@Component({
  selector: 'imprint-page',
  templateUrl: './imprint-page.component.html',
  styleUrls: ['./imprint-page.component.scss'],
})
export class ImprintPageComponent {
  ngOnInit(): void {
    window.scrollTo(0, 0);
  }
}
