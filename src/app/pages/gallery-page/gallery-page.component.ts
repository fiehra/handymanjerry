import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gallery-page',
  templateUrl: './gallery-page.component.html',
  styleUrls: ['./gallery-page.component.scss'],
})
export class GalleryPageComponent implements OnInit {
  // how to use graphixmagix
  // 1. move into project folder switched.space
  // 2. copy path and put it into ""
  // 3. run convert script inside your project folder
  // 4. node node_modules/angular2-image-gallery/convert.js <path/to/your/images>C:\Users\Fiehra\Desktop\my data\data\web development\handymanjerry\src\assets\images

  // node node_modules/angular2-image-gallery/convert.js "C:\Users\Fiehra\Desktop\my-data\data\web-development\gmGalleries\handymanJerryGallery"

  constructor() {}

  ngOnInit(): void {}
}
