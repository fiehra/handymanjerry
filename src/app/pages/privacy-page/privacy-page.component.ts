import { Component } from '@angular/core';

@Component({
  selector: 'privacy-page',
  templateUrl: './privacy-page.component.html',
  styleUrls: ['./privacy-page.component.scss'],
})
export class PrivacyPageComponent {
  ngOnInit(): void {
    window.scrollTo(0, 0);
  }
}
