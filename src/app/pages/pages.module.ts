import { NgModule } from '@angular/core';
import { ComponentsModule } from '../components/components.module';
import { HomePageComponent } from './home-page/home-page.component';
import { GalleryPageComponent } from './gallery-page/gallery-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { ImprintPageComponent } from './imprint-page/imprint-page.component';
import { PrivacyPageComponent } from './privacy-page/privacy-page.component';
import { Angular2ImageGalleryModule } from 'angular2-image-gallery';
import { HammerModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    HomePageComponent,
    GalleryPageComponent,
    NotFoundPageComponent,
    ImprintPageComponent,
    PrivacyPageComponent,
  ],
  imports: [
    ComponentsModule,
    Angular2ImageGalleryModule,
    HammerModule,
    RouterModule,
    BrowserAnimationsModule,
  ],
  exports: [],
})
export class PagesModule {}
